# alten_egg_hunt

Alten CSE :

La chasse aux oeufs de Pâques a commencé !

DU 08 AU 22 PARTICIPEZ	

à la grande chasse aux œufs de Pâques du CSE.

Connectez-vous et tentez de retrouver

les 6 œufs dissimulés sur le site.

[J'Y VAIS](https://cse.cealten.fr/com/page/473)

---

**powershell 7 :**

`300..500 | % { start https://cse.cealten.fr/com/page/$_ }`

**bookmark javascript**

`javascript:var imgg = document.querySelector('img[src*="oeuf"]')?.src; if(imgg){window.open(imgg, '_blank')}`
